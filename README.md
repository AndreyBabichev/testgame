
# Run with Docker

```
docker-compose up
```
No is available:
* Base Django Admin Interface: http://localhost:8000/admin  user: admin, password: 1q2w3e4
* Manage API via Swagger: http://localhost:8000/swagger
* VueJS + Bootstrap: http://localhost:8080


# Run backend manually

Change directory and install project requirements
```
cd backend/testgame/
pip install -r requirements.txt
```

Create superuser for Django Admin panel.

```
./manage.py createsuperuser
```

Run Django migrations to prepare database
```
./manage.py migrate
```

Run test
```
./manage.py test
```

Run test server
```
./manage.py runserver
```

Base Django Admin Interface: http://localhost:8000/admin
Manage API via Swagger: http://localhost:8000/swagger

# Run frontend base on Vue + Bootstrap manually

Change directory and install front-end requirements.
```
cd frontend/testgame/

npm install
```

Run front-end.
```
npm run serve
```

VueJS + Bootstrap: http://localhost:8080

