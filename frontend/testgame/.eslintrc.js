module.exports = {
  root: true,
  env: {
    node: true,
    browser: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    // 'semi': ['error', 'always'],
    'semi': [2, "always"],
    'one-var': 'off',
    'no-trailing-spaces': 'off',
    'padded-blocks': 'off',
    'no-mixed-operators': 'off',
    'no-extend-native': ['error', { exceptions: ['Promise'] }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
