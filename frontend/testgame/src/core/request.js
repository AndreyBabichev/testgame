import 'whatwg-fetch';

function processQueryStringValue (value) {
  value = value instanceof Date ? value.toISOString().split('T')[0] : value;
  return encodeURIComponent(value);
}

export default class Request {

  /**
   * Function which builds the query string by params.
   * @param {Object} params
   * @returns {string}
   */
  buildQueryString (params = {}) {
    let queryString = '';

    // Filter undefined values
    params = Object.entries(params).filter((entry) => entry[1] !== undefined);

    if (params.length) {
      queryString = '?' + params.map(([ key, value ]) => {
        value = processQueryStringValue(value);
        return `${key}=${value}`;
      }).join('&');
    }

    return queryString;
  }

  /**
   *
   * @param {?Object} params
   * @param {?Object} params.data
   * @param {?Object} params.queryParams
   * @param {?Number} params.page
   */
  constructor (params = {}) {
    this.params = params;
    this.headers = {
      'Content-Type': 'application/json'
    };
  }

  get uri () {
    if (!this.params.uri) {
      throw new Error('get url () is not implemented');
    }
    return this.params.uri;
  }

  get url () {
    return this.uri + this.buildQueryString(this.params.queryParams);
  }

  get body () {
    if (this.params.hasOwnProperty('data')) {
      return this.params.data;
    }
  }

  get options () {
    return {
      credentials: 'same-origin',
      method: this.method.toUpperCase(),
      body: JSON.stringify(this.body),
      headers: new Headers(this.headers)
    };
  }

  get method () {
    return this.params.method || 'get';
  }

  send () {
    const promise = fetch(this.url, this.options);
    // Hack for Safari 10
    // eslint-disable-next-line
    if (!promise.finally && promise.__proto__) {
      // eslint-disable-next-line
      promise.__proto__.finally = Promise.prototype.finally;
    }

    return promise.then((response) => {
      this.response = response;
      if (response.status !== 204) {
        return response.json();
      }
    }).then((data) => {
      const status = this.response.status;

      this.data = data;
      this.status = status;

      if (status >= 200 && status < 300) {
        return this.getData ? this.getData(data) : data;
      } else {
        console.log('ELSE');
        let error = new Error(this.response.statusText);
        console.log(data);
        error.data = data;
        error.status = status;
        throw error;
      }
    }).catch((error) => {
      throw new Error(String(error));
    });
  }

  addHeader (name, value) {
    this.headers[name] = value;
  }

}
