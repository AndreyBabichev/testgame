
const DATE_REGEXP = /^\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}:\d{2}Z)?$/;

function set (data, result = {}) {
  if (!data) {
    return data;
  }

  if (Array.isArray(data)) {
    result = data.map((el) => set(el));
  } else if (data.constructor === Object) {
    Object.keys(data).forEach((key) => {
      result[key] = set(data[key]);
    });
  } else if (data.constructor === String && data.match(DATE_REGEXP)) {
    result = new Date(data);
  } else {
    result = data;
  }
  return result;
}

export default class Model {

  constructor (data, params = {}) {
    this.update(data);

    if (params.asyncProps) {
      params.asyncProps.forEach((prop) => {
        let resolver,
          promise = new Promise(function (resolve) {
            resolver = resolve;
          });

        Object.defineProperty(this, prop, {
          get () {
            return promise;
          },
          set (data) {
            resolver(data);
          },
          configurable: true,
          enumerable: true
        });
      });
    }
  }

  update (data) {
    set(data, this);
  }

}
