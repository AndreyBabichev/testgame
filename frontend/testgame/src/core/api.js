
import Vue from 'vue';
import session from '@/data/session';

const api = {
  install () {
    Vue.prototype.$api = this;
  },

  call (request, options) {
    if (request.token) {
      return request.token.then((token) => {
        request.addHeader('Authorization', `Token ${session.token}`);
        return request.send();
      });
    } else if (session.token) {
      request.addHeader('Authorization', `Token ${session.token}`);
    }
    return request.send();
  }
};

Vue.use(api);

export default api;
