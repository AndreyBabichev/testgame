import Request from '@/core/request';

export class GetPlayersRequest extends Request {

  get uri () {
    return 'http://localhost:8000/api/players';
  }

  getData (data) {
    // return data.objects.map((data) => new User(data));
    return data.results;
  }

}

export class CreatePlayerRequest extends Request {

  get uri () {
    return `http://localhost:8000/api/players`;
  }

  get method () {
    return 'post';
  }

  getData (data) {
    return data.object;
  }

}

export class UpdatePlayerRequest extends Request {

  get uri () {
    return `http://localhost:8000/api/players/${this.params.id}`;
  }

  get method () {
    return 'patch';
  }

  getData (data) {
    return data.object;
  }

}
