import Model from '@/core/model';

const defaultSession = {
  token: null,
  name: '',
  email: ''
};

export default class Session extends Model {

  constructor (data) {
    data = data || defaultSession;
    super(data);
  }

  clear () {
    this.update(defaultSession);
  }

}
