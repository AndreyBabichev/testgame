
Build Docker image.
```
docker build -t frontend-cookbook/frontend-vuejs-testgame .
```

Tun Docker image.
```
docker run -it -p 8080:8080 --rm --name frontend-django-testgame-1 frontend-cookbook/frontend-vuejs-testgame
```
