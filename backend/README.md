
Build Docker image.
```
docker build -t backend-cookbook/backend-django-testgame .
```

Tun Docker image.
```
docker run -it -p 8000:8000 --rm --name backend-django-testgame-1 backend-cookbook/backend-django-testgame
```
