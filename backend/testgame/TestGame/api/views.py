from rest_framework.viewsets import ModelViewSet

from TestGame.api.serializers import (InventorySerializer, LocationSerializer,
                                      PlayerLocationHistorySerializer,
                                      PlayerSerializer)
from TestGame.core.models import (Inventory, Location, Player,
                                  PlayerLocationHistory)


class ApiViewMixIn:

    lookup_field = 'id'
    ordering_fields = ('id',)
    ordering = ('id',)


class PlayerModelView(ApiViewMixIn, ModelViewSet):

    queryset = Player.objects.select_related()
    serializer_class = PlayerSerializer


class InventoryModelView(ApiViewMixIn, ModelViewSet):

    queryset = Inventory.objects.select_related()
    serializer_class = InventorySerializer


class LocationModelView(ApiViewMixIn, ModelViewSet):

    queryset = Location.objects.select_related()
    serializer_class = LocationSerializer


class PlayerLocationHistoryModelView(ApiViewMixIn, ModelViewSet):

    queryset = PlayerLocationHistory.objects.select_related()
    serializer_class = PlayerLocationHistorySerializer


__all__ = (
    'PlayerModelView',
    'InventoryModelView',
    'LocationModelView',
    'PlayerLocationHistoryModelView',
)
