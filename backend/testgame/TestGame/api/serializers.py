from rest_framework.serializers import ModelSerializer

from TestGame.core.models import Player, Inventory, Location, PlayerLocationHistory


class PlayerSerializer(ModelSerializer):

    class Meta:
        model = Player
        exclude = ['password']


class InventorySerializer(ModelSerializer):

    class Meta:
        model = Inventory
        fields = '__all__'


class LocationSerializer(ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'


class PlayerLocationHistorySerializer(ModelSerializer):

    class Meta:
        model = PlayerLocationHistory
        fields = '__all__'


__all__ = (
    'PlayerSerializer',
    'InventorySerializer',
    'LocationSerializer',
    'PlayerLocationHistorySerializer',
)
