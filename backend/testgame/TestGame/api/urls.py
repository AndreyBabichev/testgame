from django.conf.urls import url
from rest_framework import routers

from .views import PlayerModelView

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'players', PlayerModelView)

urlpatterns = router.urls
