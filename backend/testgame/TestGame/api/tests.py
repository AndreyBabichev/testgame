import json
from collections import Mapping

from django.test import TransactionTestCase
from rest_framework.test import APIClient

from TestGame.core.models import Player


class BaseViewTestCase(TransactionTestCase):

    api_prefix = '/api'
    api_client = APIClient()

    token = None

    @staticmethod
    def obj_row(instance):
        return None

    def create_token(self, user_id=777):
        # TODO
        return 'x' * 32

    def setUp(self):
        super(BaseViewTestCase, self).setUp()

        self.token = self.create_token()

    def request(self, method_name, uri, qp=None, data=None):
        method = getattr(self.api_client, method_name.lower())
        headers = {}

        if self.token:
            headers['HTTP_AUTHORIZATION'] = 'Token %s' % self.token

        return method(uri, data=qp or data, format='json', **headers)

    def assertMyDictEqual(self, expected_dict, data_dict, exclude_fields=None, sorting_keys=None):
        """
        :type expected_dict: dict
        :type data_dict: dict
        :type exclude_fields: list
        :type sorting_keys: list of strings or None
        :param sorting_keys: keys to help list sorting
        """
        if sorting_keys is None:
            sorting_keys = []

        for field, expected_value in list(expected_dict.items()):
            if exclude_fields and field in exclude_fields:
                continue
            try:
                value = data_dict[field]
            except KeyError:
                raise self.failureException(
                    (
                        "Matched dictionary does not contain the key '%s'\n"
                        "Expected: %s\n"
                        "Matched: %s\n"
                    ) % (field, expected_dict, data_dict),
                )

            try:
                self.assertMyEqual(
                    expected_value,
                    value,
                    exclude_fields=exclude_fields,
                    sorting_keys=sorting_keys,
                )
            except self.failureException as err:
                raise self.failureException(str(err) + " field '%s'" % field)

        expected_keys = list(expected_dict.keys())
        data_keys = list(data_dict.keys())

        if len(expected_keys) != len(data_keys):
            missed_keys = []
            exclude_fields = exclude_fields or ()
            for k in data_keys:
                if k not in expected_dict and exclude_fields and k not in exclude_fields:
                    missed_keys.append(k)
            if missed_keys:
                raise self.failureException("The expected value missed next keys: %s." % missed_keys)

    def assertMyListEqual(self, expected_list, data_list, exclude_fields=None, sorting_keys=None):
        """
        :type expected_list: list or tuple
        :type data_list: list or tuple
        :type exclude_fields: list
        :type sorting_keys: list of strings or None or tuple
        :param sorting_keys: keys to help list sorting
        """
        if sorting_keys is None:
            sorting_keys = []

        if len(expected_list) != len(data_list):
            raise self.failureException("Expected %s elements, provided %s" % (len(expected_list), len(data_list)))

        if not len(expected_list):
            return

        def value_extractor(obj):
            if not isinstance(obj, Mapping):
                return obj

            for k in sorting_keys:
                v = obj.get(k)

                if v is None:
                    continue

                return v

            return obj

        def key_extractor(obj):
            if isinstance(obj, tuple):
                return obj[0]

            if isinstance(obj, dict):
                return sorted(list(obj.keys()))

            if isinstance(obj, int):
                return str(obj)

            if obj is None:
                return 'None'

            return obj

        if not sorting_keys:
            key_function = key_extractor
        else:
            key_function = value_extractor

        expected_list = sorted(expected_list, key=key_function)
        data_list = sorted(data_list, key=key_function)

        for n in range(len(expected_list)):
            expected_item = expected_list[n]
            data_item = data_list[n]

            try:
                self.assertMyEqual(expected_item, data_item, exclude_fields=exclude_fields, sorting_keys=sorting_keys)
            except self.failureException as err:
                raise self.failureException(
                    str(err) + " at %s item,\nExpected: %s\nOrigin: %s" %
                    (n, expected_item, data_item),
                )

    def assertMyEqual(self, expected_obj, data_obj, exclude_fields=None, sorting_keys=None):
        """
        :type expected_obj: object
        :type data_obj: object
        :type exclude_fields: list
        :type sorting_keys: list or tuple or None
        :param sorting_keys: keys to help list sorting
        """
        if sorting_keys is None:
            sorting_keys = []

        if isinstance(expected_obj, dict):
            assert isinstance(data_obj, dict), "second argument must be 'dict'"

            self.assertMyDictEqual(
                expected_obj,
                data_obj,
                exclude_fields=exclude_fields,
                sorting_keys=sorting_keys,
            )
        elif isinstance(expected_obj, (list, tuple)):
            assert isinstance(data_obj, (list, tuple)), "second argument must be 'list' or 'tuple'"

            self.assertMyListEqual(
                expected_obj,
                data_obj,
                exclude_fields=exclude_fields,
                sorting_keys=sorting_keys,
            )
        else:
            self.assertEqual(expected_obj, data_obj)

    def assertDatetimeEqual(self, datetime1, datetime2, fuzz_in_seconds=2):
        """
        Compare datetime objects with respect that small difference between them can occur.

        :type datetime1: first datetime object
        :type datetime2: second datetime object
        :type fuzz_in_seconds: maximum seconds can be between datetime1 and datetime2 and result will be equality
        """
        return abs((datetime1 - datetime2).total_seconds()) < fuzz_in_seconds


class BaseModelViewTestCase(BaseViewTestCase):

    objects_uri = NotImplemented
    object_uri = NotImplemented

    empty_response = {'results': [], 'count': 0}

    @property
    def create_object_uri(self):
        return self.objects_uri

    def get_objects(self, **kwargs):
        return self.request('get', self.objects_uri, **kwargs)

    def create_object(self, data):
        return self.request('post', self.create_object_uri, data=data)

    def get_object(self, identity, **kwargs):
        return self.request('get', self.object_uri % identity, **kwargs)

    def update_object(self, identity, **kwargs):
        return self.request('put', self.object_uri % identity, **kwargs)

    def partial_update_object(self, identity, data):
        return self.request('patch', self.object_uri % identity, data=data)

    def delete_object(self, identity):
        return self.request('delete', self.object_uri % identity)

    def test_empty_list(self):
        if self.objects_uri == NotImplemented:
            return

        self.assertResponseJsonOk(self.empty_response, self.get_objects())

    def assertResponseOk(self, response, expected_data=None):
        self.assertMyEqual(response.status_code, 200, response)
        if expected_data is not None:
            self.assertMyEqual(response.json_data, expected_data)

    def assertResponseCreated(self, response, expected_data=None):
        self.assertMyEqual(response.status_code, 201, response)
        if expected_data is not None:
            self.assertMyEqual(response.json_data, expected_data)

    def assertResponseJson(self, expected_data, response, sorting_keys=None):
        result = json.loads(response.content)
        self.assertMyEqual(expected_data, result, sorting_keys=sorting_keys)

    def assertResponseJsonOk(self, expected_data, response, sorting_keys=None):
        self.assertResponseOk(response)
        self.assertResponseJson(expected_data, response, sorting_keys=sorting_keys)

    def assertResponseJsonCreated(self, expected_data, response, sorting_keys=None):
        self.assertResponseCreated(response)
        self.assertResponseJson(expected_data, response, sorting_keys=sorting_keys)

    def assertObjectsResponse(self, response, objects=None, total=0, sorting_keys=None):
        obj_list = ()
        if isinstance(objects, (tuple, list)):
            obj_list = objects
        elif objects is not None:
            obj_list = (objects, )

        self.assertResponseJsonOk(
            {
                "results": [self.obj_row(obj) for obj in obj_list],
                "count": total,
            },
            response,
        )

    @staticmethod
    def multi_qp(*objects, key='id'):
        """
        Converts a list of objects to a string containing those objects' identifiers, comma separated,
        using a key
        """
        return ','.join(map(lambda o: str(getattr(o, key)), objects))


class PlayerModelViewTestCase(BaseModelViewTestCase):

    objects_uri = BaseModelViewTestCase.api_prefix + '/players'
    object_uri = BaseModelViewTestCase.api_prefix + '/players/%s'

    @staticmethod
    def obj_row(test_obj):
        """
        :type test_obj: TestGame.core.models.Player
        :rtype: dict
        """
        return {
            'id': test_obj.id,
            'name': test_obj.name,
            'clan': test_obj.clan,
        }

    def setup_fixtures(self):
        self.test_user1 = Player.objects.create(name='Player1', clan='TestClan')

    def test_select_all(self):
        self.setup_fixtures()

        # Select all
        self.assertObjectsResponse(
            self.get_objects(),
            objects=self.test_user1,
            total=1,
        )

    def test_create_new(self):
        self.assertResponseJsonCreated(
            {'id': 1, 'name': 'New Player', 'clan': 'New Clan'},
            self.create_object({'name': 'New Player', 'clan': 'New Clan'}))


__all__ = (
    'PlayerModelViewTestCase',
)
