from django.contrib import admin
from TestGame.core.models import Player, Inventory, Location, PlayerLocationHistory

admin.site.register(Player)
admin.site.register(Inventory)
admin.site.register(Location)
admin.site.register(PlayerLocationHistory)
