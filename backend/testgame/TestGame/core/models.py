import re
from _md5 import md5

import bcrypt
from django.conf import settings
from django.db import models
from django.utils.encoding import smart_bytes

RE_PASSWORD = re.compile(r'([A-Fa-f0-9]{32})')


class BaseGameModel(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class PlayerQuerySet(models.QuerySet):

    def delete(self):
        raise Exception("Player can't be deleted.")


class PlayerManager(models.Manager):

    def get_queryset(self):
        return PlayerQuerySet(self.model, using=self._db)


class Player(BaseGameModel):

    name = models.CharField(max_length=60)
    password = models.CharField(
        max_length=60,
        null=False,
    )
    clan = models.CharField(max_length=60)
    killed = models.PositiveIntegerField(null=False, default=0)
    last_login_at = models.DateTimeField(null=True)

    objects = PlayerManager()

    def set_password(self, password):
        # TODO use it on save!
        if password is not None:
            if not RE_PASSWORD.match(password):
                password_md5 = md5(smart_bytes(password)).hexdigest()
            else:
                password_md5 = password

            salt = bcrypt.gensalt(settings.BCRYPT_STRENGTH_LEVEL)

            self.password = bcrypt.hashpw(smart_bytes(password_md5), salt)
        else:
            # reset password
            self.password = ''

    def __str__(self):
        return 'Player(id=%s, name=%s)' % (self.id, self.name)

    def delete(self, using=None, keep_parents=False):
        raise Exception("Player can't be deleted.")


class Inventory(BaseGameModel):
    DESP_BELT = 1
    DESP_BACKPACK = 2
    DESP_STORAGE = 3
    DESP_CHOICES = [
        (DESP_BELT, 'Belt'),
        (DESP_BACKPACK, 'Backpack'),
        (DESP_STORAGE, 'Storage'),
    ]

    player = models.ForeignKey(Player, models.CASCADE)
    name = models.CharField(max_length=60, null=False, default='')
    usage = models.PositiveIntegerField(null=False, default=0, help_text="Usage percentage")
    disposition = models.PositiveSmallIntegerField(choices=DESP_CHOICES, null=False, default=DESP_BACKPACK)

    def __str__(self):
        return 'Inventory(id=%s, name=%s, player_id=%s)' % (self.id, self.name, self.player_id)


class Location(BaseGameModel):

    name = models.CharField(max_length=60)

    def __str__(self):
        return 'Location(id=%s, name=%s)' % (self.id, self.name)


class PlayerLocationHistory(BaseGameModel):

    player = models.ForeignKey(Player, models.CASCADE)
    location = models.ForeignKey(Location, models.CASCADE)

    def __str__(self):
        return 'PlayerLocationHistory(id=%s, player_id=%s, location_id=%s)' % (
            self.id, self.player_id, self.location_id)


__all__ = (
    'Player',
    'Inventory',
    'Location',
    'PlayerLocationHistory',
)
