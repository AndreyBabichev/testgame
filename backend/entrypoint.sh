#!/bin/bash

# Collect static files
# echo "Collect static files"
# python manage.py collectstatic --noinput

rm TestGame/db.sqlite3

# Apply database migrations
echo "Apply database migrations"
python manage.py makemigrations
python manage.py migrate

echo "Setup superuser password: 1q2w3e4"
python manage.py shell -c "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'testgame-admin@minuteinbox.com', '1q2w3e4')"

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000